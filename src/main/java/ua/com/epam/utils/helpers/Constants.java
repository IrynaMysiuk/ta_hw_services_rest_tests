package ua.com.epam.utils.helpers;

import java.util.Arrays;
import java.util.List;

public interface Constants {
    List<String> EXPECTED_QUERIES = Arrays.asList("Car", "Ber", "Marce", "Wil", "Rud");
    int TEST_EXPECTED_SIZE = 5;
}
